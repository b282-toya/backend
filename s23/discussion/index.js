// [SECTION] Objects
// An object is a data type that is used to represent real world objects
// Information stored in objects are represented in a "Key: Value" pair

// Creating objects using object initializers/literal notation
/*
SYNTAX:
	let objectName = {
		keyA: valueA,
		keyB: valueB,
		.....
	}

*/


let cellphone = {
	name: 'Nokia3210',
	manufactureDate: 1999,
};


console.log("Result from creating objects using literal notation:");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
/*
SYNTAX:
	function ObjectName(keyA, keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}
*/

function Laptop(name, manufactureDate) {
		// "this" keyword allows to assign a new object's properties by associating them with values received from constructor function's parameter
		this.name = name;
		this.manufactureDate = manufactureDate;
	}
// invokation of function
	// "new" operator creates an instance of an object
let laptop = new Laptop("Lenovo", 2008);
console.log(laptop);


let myLaptop = new Laptop("Macbook Air", 2020);
console.log(myLaptop);

// [SECTION] Accessing object properties
// Using dot notation
console.log(myLaptop.name);

// Using square bracket notation
console.log(myLaptop['manufactureDate']);


// Accessing array objects
let array =[laptop, myLaptop];

// square bracket notation
console.log(array[0]['name']);
// dot notation
console.log(array[0].manufactureDate);

// [SECTION] Initializing/Adding, Deleting, Reassigning Object properties

let car ={};


// Initializing/adding object properties using dot notation
car.name = "Honda Civic";
console.log(car);

car.manufactureDate = 2020;
console.log(car);

// Initializing/adding object properties using square bracket notation
car['country'] = 'Japan';
console.log(car);

// deleting object properties
delete car['manufactureDate'];
console.log(car);

delete car.country;
console.log(car);


// Reassigning object properties
car.name ="Dodge Charger R/T";
console.log(car);

car['manufactureDate'] = 2023;
console.log(car);

// [SECTION] Object Methods
// A method is a function which is a property of an objects

let person = {
	name: "Pochi",
	talk: function() {
		console.log("Hello, my name is " + this.name);
	}
}

console.log(person);
person.talk();

// [SECTION] Real World Application of Objects
// Using object literals

let myPokemon = {
	name: "Pikachu",
	level: 99,
	health: 1000,
	attack: 123,
	// para maging method maglalagay ng anonymous function
	tackle: function() {
		console.log('This Pokemon tackled targetPokemon');
	},
	faint: function() {
		console.log('Pokemon Fainted');
	}
}

console.log(myPokemon);
myPokemon.tackle();
myPokemon.faint();



// Using constructor function

function Pokemon (name, level) {

	// properties
	this.name = name;
	this.level = level; // 10
	this.health = 2 * level; // 20
	this.attack = level;

	// method
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log('targetPokemon\'s health is now reduced to '+ Number(target.health - this.attack));
	};
	this.faint = function() {
		console.log(this.name + 'fainted.');
	}
};

let pikachu = new Pokemon('Pikachu', 20);
console.log(pikachu);

let onyx = new Pokemon('Onyx', 10);
console.log(onyx);

pikachu.tackle(onyx);











