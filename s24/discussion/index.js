// [SECTION] Exponent Operator

console.log("Exponent Operator:");
// Math.pow (old ver.) method takes two arguments, the base and the exponent
// raise 8 to the power of 2
const firstNum = Math.pow (8,2); // 64
console.log(firstNum);

// ** (new ver.) Exponential Operator is used for exponentiation

const secondNum = 8 ** 2; // 64
console.log(secondNum);

// [SECTION] Template Literals
// Allows to write strings without using concatenation operator "+"

let name = "Pochi";

let message = "Hello " + name + '! Welcome to programming.';

console.log(message);

// Uses back ticks ` `
message = `Hello ${name}! Welcome to programming.`;
console.log(message);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${interestRate*principal}`);


// [SECTION] Array Destructuring
// Allows to unpack elements in arrays into distinct variables

/*
	let/const [variableName, variableName, variableName...] = arrayName;
*/


const fullName = ['Juan', 'Dela', 'Cruz'];
console.log(fullName[1]); // Dela

const [firstName, middleName, lastName] = fullName;
console.log(middleName); // Dela


// [SECTION] Object Destructuring
// Allows to unpack elements in objects into distinct variables

/*
	let/const {propertyName, propertyName, propertyName...} = objectName;
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

console.log(person.familyName); // Cruz

const {givenName, maidenName, familyName} = person;
console.log(familyName); // Cruz


// [SECTION] Arrow Functions
// Arrow functions allow us to write shorter function syntax
/*
SYNTAX:
	let/const variableName = () => {
		statement
	}

*/

const students = ["John", "Jane", "Judy"];

students.forEach(function(student) {
	console.log(`${student} is a student.`);
});
// Arrow Function
console.log(`Result from using Arrow function:`);
students.forEach((student) => {
	console.log(`${student} is a student.`);
});

// [SECTION] Implicit Return Statement
// There are instances when you can omit the "return" statement

// const add = function(x, y) {
// 	return x + y;
// }
//  const total = add(1, 2);
//  console.log(total);
// reminder test natin ito tosh sa mga lumang activity

 // Arrow Function

const add = (x, y) => x + y;
const total = add(1, 2);
console.log(total);

// [SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked


// meron curly braces {} dahil meron return statement kapag wala mag-error; puwede dumiretso ng wala ang return value.
// user = assignment operator
const greet = (name = "User") => {
	return `Good morning, ${name}!`;
}
console.log(greet());
console.log(greet("Pochi"));


// [SECTION] Class-Based Object Blueprints
// Allows creation/instatiation of objects using classes as blueprints

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

// Creating/Instantiating a new object from car class with initialized values
const myNewCar = new Car("Subaru", "x1", 2023);
console.log(myNewCar);












