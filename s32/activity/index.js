let http = require("http");

const app = http.createServer(function (request, response) {
    //Add code here
    response.writeHead(200, { "Content-Type": "text/plain" });
    
    if (request.url == "/" && request.method == "GET") {
        response.write("Welcome to Booking System");
    }

    if (request.url == "/profile" && request.method == "GET") {
        response.write("Welcome to your profile!");
    }
    
   if (request.url == "/courses" && request.method == "GET") {
        response.write("Here's our courses available");
    }

   if (request.url == "/addcourse" && request.method == "POST") {
        response.write("Add a course to our resources");
    }    

   if (request.url == "/updatecourse" && request.method == "PUT") {
        response.write("Update a course to our resources");
    }

   if (request.url == "/archivecourses" && request.method == "DELETE") {
        response.write("Archive courses to our resources");
    }
    response.end();
});


//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;