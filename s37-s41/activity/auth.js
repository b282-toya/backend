const jwt = require("jsonwebtoken");
// secret is user defined string data that will be used to create our JSON web tokens
const secret = "CourseBookingAPI";

// [SECTION JSON Web Token]

// Token Creation
module.exports.createAccessToken = (user) => {
	// payload - reason of importance: for indetification for authentication
	const data = {
		// id = unique identifier
		id: user._id,
		// email = for log
		email: user.email,
		// isAdmin = for access via different level of user for security verification
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
};

// Token verification
module.exports.verify = (req, res, next) => {
	// pinasok sa token ang value ng headers auth
	let token = req.headers.authorization;
	// !== strictly not equal
	if (typeof token !== "undefined") {
		console.log(token); //Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCYzI2OCIsImVtYWlsIjoiamFuZUBtYWlsLmNvbSIsImlzQWRtaWMDR9.GlUBUnAZ7xHPo_UXqA2NOWU9HJ78KWJntQUicbMMXxE


		token = token.slice(7, token.length);
		// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YTJhM2U0ZWQ2OTUzNDllYWY4YzI2OCIsImVtYWlsIjoiamFuZUBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2ODgzODA0MDR9.GlUBUnAZ7xHPo_UXqA2NOWU9HJ78KWJntQUicbMMXxE
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth:"failed"});
			} else {
				next() // Allow the application to proceed to the next middleware (module.exports.decode) callback function in the route
			};
		});
	} else {
		return res.send({auth: "failed"});
	};
};

// Token Decryption
module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			};
		});
	} else {
		return null;
	};
};

// Admin Token for course


// module.exports.verifyAdmin = (req, res, next) => {
//   let token = req.headers.authorization;

//   if (typeof token !== "undefined") {
//     token = token.slice(7, token.length);

//     jwt.verify(token, secret, (err, decoded) => {
//       if (err || !decoded.isAdmin) {
//         return res.send({ auth: "Unauthorized Access" });
//       } else {
//         next();
//       }
//     });
//   } else {
//     return res.send({ auth: "Unauthorized Access" });
//   }
// };
