const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our frontend application
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

const app = express();

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://toshitoyajr:0WpQikvs8E6C5HSa@wdc028-course-booking.cmjynyq.mongodb.net/courseBookingAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Defines the "/users" string to be included for all user routes defined in the "userRoute" file
// http://localhost:4000/users
app.use("/users", userRoute);
// http://localhost:4000/courses
app.use("/courses", courseRoute);



// "process.env.PORT" is an environment variable that typically holds the port number on which the server should listen.
app.listen(process.env.PORT || 4000, () => console.log(`Now listening to port ${process.env.PORT || 4000}!`));