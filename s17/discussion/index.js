// console.log("Hello World!");

// [SECTION] Functions
// Functions in JS are lines/block of codes that tell our device/application to perform a certain task when called/invoked

// Function declarations
/*
SYNTAX:
	function functionName() {
		code block (statement)
	}
*/

// function - keyword used to define a JS functions
// printName - function name. Functions are named to be able to be used later in the code

function printName() {
	// function block {} - the statements which comprise the body of the function
	console.log("My name is Pochi");
}

// Function invocation
printName();

// [SECTION] Function declarations vs expressions
// Function Declarations
// A function can be created through function declaration by using FUNCTION keyword and adding a function name
function declaredFunction(){
	console.log("Hello World from declaredFunction()!")
}
declaredFunction();

// Function Expressions
// A function can also be stored in a variable
// A function expression is an anonymous function assigned to a variableFunction
// anonymous function , no name

// variableFunction(); //Uncaught ReferenceError: Cannot access 'variableFunction' before initialization hindi puwede ang hoisting

let variableFunction = function () {
	console.log("Hello again!");
}

variableFunction(); 

// Function Expressions are always invoked/called using variable name
let funcExpression = function funcName() {
	console.log("Hello from the other side!");

}

// funcName(); //Uncaught ReferenceError: funcName is not defined - hindi puwede gamitin
funcExpression();


// You can reassign function declarations and function expressions to new anonymous functions

declaredFunction = function() {
	console.log("Updated declaredFunction!");
}

declaredFunction();

funcExpression = function(){
	console.log("Updated funcExpression!");
}
funcExpression();

// declaration - function name; expression- variable name

const constantFunc = function() {
	console.log("Initialized with const!");
}

constantFunc();

// constantFunc = function() {
// 	console.log("Cannot be reassigned!");
// }

// constantFunc(); //Uncaught TypeError: Assignment to constant variable.

// [SECTION] Function scoping
// Scope is the accessibility/visibility of variables
// JS has 3 type of scope
/*
1. local/block scope
2. global scope
3. function scope
*/
{
	let localVar = "Armando Perez";
	console.log(localVar);
}

let globalVar = "Mr. Globe";

console.log(globalVar);
// console.log(localVar); // hindi puwede lumabas sa block/local scope

function showNames() {

	// function scoped variables
	var functionVar = "Yo";
	const functionConst = "Belly";
	let functionLet = "Damo";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();


// Global Scope 
let globalName = "Alexandro"

function myNewFunction2() {
	console.log(globalName)
}

myNewFunction2();

// Nested Functions
// You can create another function inside a function

function myNewFunc () {
	let name = "Rizza";

	function nestedFunction() {
		let nestedName = "Sushi"
		console.log(nestedName);
	}
	nestedFunction();
}
myNewFunc();

// [SECTION] Using alert()
// alert() allows us to show a small window at the top of our browser page to show information to our users

// alert("Hello World!"); //This will run immediately when the page loads

// function showSampleAlert() {
// 	alert("Hello, User!")
// }
// showSampleAlert();

// console.log("I will only log in the console when the alert is dismissed.");

// [SECTION] Using prompt()
// prompt() allows us to show a small window at the top of the browser to gather user input


let samplePrompt = prompt("Enter your name");
console.log("Hello, " + samplePrompt);
// ok without string = blank; cancel = null

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name:");
	let lastName = prompt("Enter your last name.");

	console.log("Hello, " + firstName + " " + lastName + "!");
		// Hello, Pochi Kun !
	console.log("Welcome to my page!");
}
printWelcomeMessage();

// [SECTION] Function Naming Conventions

// Function names should be definitive of the task it will perform.
// It usually contains a verb
function getCourses() {
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);

}
getCourses();

// Name your functions in small caps
// Follow camelCase when naming variables
function displayCarInfo() {
	let carInfo = ["Mazda", "Honda", "Toyota"];
	console.log(carInfo);
}
displayCarInfo();

// Avoid generic names to avoid confusion within your code
function get() {
	console.log("Huwag")
}
get();

// Avoid pointless and inappropriate function name
function foo() {
	console.log("Ambot")
}
foo();