// S29 - MongoDB Query Operators and Field Projection:

/*

    Sample solution:

    return async function findName(db) {
        await (db.collectionName.find({
            $and: [
            	{field1: "value1"},
             	{field2: "value2"}
            ]
        }));
        
    }

Note: 
	- Do note change the functionName or modify the exports
	- Delete all the comments before pushing.

*/


db.users.insertMany([
    {
        firstName: "John",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
        courses: [ "CSS", "Javascript", "Python" ],
        department: "none"
    },
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },

]);






// 1. Find users with letter s in their first name or d in their last name.
async function findName(db) {
	return await(

			// Add query here
       db.users.find(
       {
            $or: [
             { firstName: { $regex: 's',$options: 'i' } },
             { lastName: { $regex: 'd',$options: 'i' } }
            ]
        },

        { firstName: 1, lastName: 1, _id: 0 }
    );


		);

};

// 2. Find users who are from the HR department and their age is greater than or equal to 70. Use $and operator
async function findDeptAge(db) {
	return await (

			// Add query here
			db.users.find(
                {
                    $and: [
                        {department: "HR"}, 
                        {age: {$gte: 70}}
                    ]
                 });
		);

};


// 3. Find users with the letter e in their last name and has an age of less than or equal to 30. Use the $and, $regex and $lte operators
async function findNameAge(db) {
	return await (

			// Add query here
			db.users.find(
                {
                    $and: [  
					{lastName: {$regex: 'e',$options: 'i'}}, 
                    {age: {$lte: 30}}
                    ]
				});
		);
};


try{
    module.exports = {
        findName,
        findDeptAge,
        findNameAge
    };
} catch(err){

};
