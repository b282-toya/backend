Hi B282!
//Create a brand new database called - "session-recap"
//After creating your database, you may now create these documents

//1. CREATE

//Create one document

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "none"
});

//Insert multiple documents

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "none"
    }
]);

//2.RETRIEVE

//This will retrieve all the documents?
//db.users.find()

//Retrieve a document that has Stephen as its firstName
db.users.find({ "firstName": "Stephen" });

//Retrieve document/s with Armstrong as its lastName and 82 as its age
db.users.find({ "lastName" : "Armstrong", "age" : 82});



db.getCollection('users').find({ "lastName":"Armstrong", "age": 82});




3. UPDATE

// We are now going to crate a document to pudate
// Creating a document to update

db.users.insertOne({
    firstName: "Test",
  lastName:"Test",
  age: 0,
  contact:{
    phone: "00000000",
    email:"test@gmail.com"
  },
  courses:[],
  department:"none"
});

// Update a document with a specific criteria
db.users.updateOne(
    {firstName: "Test"},
    {
        $set:{
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "bill@gmail.com"
            },
            course: ["PHP","Laravel","HTML"],
            department: "Operations",
            status: "active"
    }
  }
);

// Update multiple documents

db.users.updateMany(
    
  {department: "none"},
  {
    $set: {department: "HR"}
  }
);

// we can also replace the whole document

db.users.replaceOne(
{firstName: "Bill"},
{
    firstName: "Bil",
    lastName: "Gates",
    age: 65,
    contact: {
        phone: "12345"
        email: "bill@gmail.com"
    },
    course: ["PHP","Laravel","HTML"],
    department: "Operations",
}

);

//4. DELETE

//create a document we are going to delete

db.users.insertOne({
    firstName: "test"
})

//How can we delete a single document with test as its firstname? 
db.users.deleteOne({
    firstName: "test"
})

//How can we delete multiple documenst with Bill as its firstName?

db.users.deleteMany({ "firstName": "Bil"});


//Advanced Queries

// Query an embedded document
db.users.find({
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    }
});

// Query on nested field
db.users.find(
    {"contact.email": "janedoe@gmail.com"}
);

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } );

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } );

// Querying an Embedded Array
db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

db.users.find({
    namearr: 
        {
            namea: "juan"
        }
});




// [SECTION] MongoDB - Query Operators and Field Projection


// [SECTION] Comparison Query Operators

// $gt / $gte operator

db.users.find({age: {$gt : 50}});
db.users.find({age: {$gte : 50}});

// $lt / $lte operator

db.users.find({age: {$gt : 50}});
db.users.find({age: {$gte : 50}});



// $ne operator
/*
    Allows us to find documents that have field number values not equal to a specified value
    -Syntax
    db.collectionName.find({field: {$ne: value}})
*/

db.users.find({age: {$ne: 82}});


// $in operator
/*
    Allows us to find documents with specific match criteria one field using different values
    -Syntax
    db.collectionName.find({field: {$in: value}})
*/

db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});


// [SECTION] Logical Query Operators

// $or operator
/*
    Allows us to find documents that match a single criteria from multiple provided search criteria
    -Syntax
    db.collectionName.find({$or: [{fieldA: valueA},{fieldB: valueB}]})
*/

db.users.find({$or: [{"firstName":"Neil"},{age: 21}]});

// $and operator
/*
    Allows us to find documents matching multiple criteria in a single field
    -Syntax
    db.collectionName.find({$and: [{fieldA: valueA},{fieldB: valueB}]})
*/

db.users.find({$and: [{age:{$ne:82}},{age:{$ne:76}}]});


// [SECTION] Field Projection

// Inclusion
/*
    -Allows us to include/add specific fields only when retrieving documents
    -the value provided is 1 to denote that the field is being included
    -Syntax
        db.users.find({criteria},{field:1})
*/

db.users.find(
    {firstName: "Jane"},
    {firstName: 1, lastName: 1, contact: 1}
);

// Exclusion
/*
    Allows us to exclude/remove specific fields only when retrieving documents
    -the value provided is 0 to denote that the field is being excluded
       -Syntax
        db.users.find({criteria},{field:0})
*/

db.users.find(
    {firstName: "Jane"},
    {contact: 0, department: 0}
);


// Suppressing the ID field
/*
    Allows us to exclude the "_id" field when retrieving documents
    When we are using field projection, field inclusion and exclusion may not be used at the same time
    -Excluding the "_id" field is the only exception to this rule
    -Syntax
    db.users.find({criteria},{_id:0})
*/

db.users.find(
    {firstName: "Jane"},
    {firstName: 1, lastName: 1, contact: 1, _id: 0}
);