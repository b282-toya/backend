const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

const app = express();

const port = 3001;

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://toshitoyajr:0WpQikvs8E6C5HSa@wdc028-course-booking.cmjynyq.mongodb.net/B282-s36-discussion",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Add the tasks route
// Allows all the task routes created in the "taskRoute.js" to use "/tasks" route or resources
// http://localhost:3001/tasks
app.use("/tasks", taskRoute);





app.listen(port, () => console.log(`Server running at port ${port}!`));