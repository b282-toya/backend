// [SECTION] Syntax, Statements, and Comments

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code


/*
There are two types of comments:
	1. Single-line comment -denoted by two slashes
	2. Multi-line - denoted by a slash and asterisk
*/

// Statements
// Statements in programming are instructions that we tell the computer to perform
console.log("Hello World!");

// Whitespace
// Whitespace (basically, spaces and line breaks) can impact functionality in many computer languages but NOT in JS.
console.log ( "Hello PH!" ) ;

console.

log

(
	"Hello Handsome!"
	);

// Syntax
// Syntax in programming, it is the set of rules that describes how statements must be constructed.



// [SECTION] Variables
// It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"

// Declaring variables - tells our devices that a variable name is created and is ready to store data

/*
SYNTAX:
	let/const variableFirstName;
*/

let myVariables;
console.log(myVariables);

/*// bawal ito
console.log(hello);
let hello;
*/
/*
Naming Convention
Camel Case: Words are joined together without spaces, and each word, except the first one, starts with a capital letter. For example, myVariableName.

Pascal Case: Similar to camel case, but the first letter of each word is capitalized. For example, MyVariableName.

Kebab Case: Words are joined together with hyphens (-) and all letters are lowercase. For example, my-variable-name.

Snake Case: Words are written in all lowercase letters and separated by underscores (_). For example, my_variable_name.
*/


// Declaring and Initializing variables
// Initializing variables - the instance when a variable is given it's initial value/starting value

/*
SYNTAX:
	// let/const variableName = value;
*/

let productName = "desktop computer";
console.log(productName); // result: desktop computer

let productPrice = 18999;
console.log(productPrice); // result: 18999

const interest = 3.539;
console.log(interest);

// Reassigning variable values
// means changing its initial value or previous value into another value

/*
SYNTAX:
	variableName = newValue;
*/

// di puwede maulit ang let productName

productName = "Laptop";
console.log(productName); //result: Laptop instead of desktop computer

// const variable cannot be updated or re-declared
// interest = 4.444;

// Reassigning variables vs Initializing variables
// Declaration

let supplier;

// Initialization
supplier = "John Smith Tradings";
console.log(supplier); // result: John Smith Tradings

// Reassignment

supplier = "Tokwa Factory";
console.log(supplier); 


// var - is also in declaring a variable (can be mixed and not used nowadays due to unpredictability and bugs)
// Hoisting is JS default behavior of moving initialization to top
a = 5;
console.log(a);
var a;

// a = 5;
// console.log(a); //caught SyntaxError: Identifier 'a' has already been declared (
// let a;

// let/const local/global scope
// Scope essentially means where these variables are available for use
// let and const are blocked scoped
// A block is a chunk of code bounded by {}


let outerVariable = "Hello";
{
	let innerVariable = "Hello again";
	console.log(innerVariable); // by default synchronous per line dun siya magpprint kung asan si log
}
console.log(outerVariable);
// console.log(innerVariable); // result: Uncaught ReferenceError: innerVariable is not defined



// Multiple variable declaration
let productCode = 'DC017', productBrand = 'Dell'
console.log(productCode, productBrand);


// Using a variable with a reserved keyword
// const let = "hello";
// console.log(let); // result: Uncaught SyntaxError: let is disallowed as a lexically bound name

// [SECTION] Data types
// Strings are series of characters that create a word, phrase, sentence or anything related to creating text
// Strings in JS can be writted using either a single ('') or (" ") quote
let country = 'Philippines';
let province = "Metro Manila";
let motto = 'More fun in Home';

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol
let fullAddress = province +", "+ country +" "+motto;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Escape character (\)
// "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\n\nPhilippines';
console.log(mailAddress);

let message ="John's employees went home early";
console.log(message);

message = '"Hi"'
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

message	= "\\";
console.log(message);

// Number
// Integer/Whole Numbers
let headcount = 26; //only string, arrays use ""
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining number data type and strings
console.log('John\'s grade last quarter is '+ grade);
console.log("John's grade last quarter is "+ grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
let isMarried = false;
let inGoodConduct =true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// Arrays are special kind of object data type that's used to store multiple values
/*
SYNTAX:
	let/const arrayName = [elementA, ElementB, elementC, ..]
*/

// Similar data types
let grades = [98.7, 99.9, 90.7, 89.1];
console.log(grades);

// different data types (NOT RECOMMENDED)
let details = ["Tokwa", "Tofu", 10, true];
console.log(details);

// Objects is a special kind of data type that's used to mimic real world objects/items
/*
SYNTAX:
	let/const objectName = {
		propertyA: value,
		propertyB: value,
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+63912 345 6789", "9873 1234"],
	address: {
		houseNumber: '123',
		city: 'Manila'
	}

}


console.log(person);

console.log(typeof grade);
console.log(typeof grades);
console.log(typeof person);

// Null
// it is used to intentionally express the absence of a value in a variable declaration/initialization
let spouse = null;
console.log(spouse);

// Undefined
// Represents the state of a variable that has been declared but without an assigned value
let fullName;
console.log(fullName);