// console.log("Hello World!");

// [SECTION] While Loop
// for numbers only/counting
/*
SYNTAX:
	while(expression/condition) {
		statement
	}
*/

let count = 5;

// while the value of count is not equal to zero
while(count !== 0) {
	// the current value of count is printed out
	console.log("While " + count);
	// decrease the value by 1 after every iteration to STOP the loop when it reaches 0
	count--;
}


// [SECTION] Do While Loop
// A do-while loop works a lot like while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once

/*
SYNTAX:
	do {
		statement
	} while (expression/condition)
*/

// Number() - makes it 0
/*let number = Number(prompt("Give me a number: "));*/

/*do {
	// The current value of number is printed out
	console.log("Do While: " + number);
	// Increase the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater than 10
	number += 1;
	// Providing a number of 10 or greater will run the code block once and will stop the loop
} while (number < 10);*/

// [SECTION] For Loop
/*
// A for loop is more flexible than while and do-while loops. It consists of three parts:
1. The "initialization" value that will track the progression of the loop.
2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
3. The "finalExpression" indicates how to advance the loop.
*/
/*
SYNTAX:
	for(initialization; expression/condition; finalExpression) {
		statement
	}
*/


for (let count = 0; count <= 20; count++) {
	// the current value of count is printed out
	console.log(count);
}

// .length property
// Characters in string may be counted using the .length property
// The first character in a string corresponds to the number 0, the next is 1 up to the nth number
let myString = "maria chloe";
console.log("Result of .length: " + myString.length);

// Accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);
console.log(myString[4]);
console.log(myString[5]);

// Loop that will print our the individual letter of the myString variable
// i - index
for (let i = 0; i < myString.length; i++) {
	// the current value of myString is printed out using its INDEX value
	console.log(myString[i]);
}


// Loop that will print out the letters of the name individually and print out the number 3 instead  when the letter to be printed out is a vowel

let myName ="AlEx";

for (let i = 0; i < myName.length; i++) {

	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		) {
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}

// [SECTION] Continue and Break Statements
// The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
// The "break" statement is used to terminate the current loop once a match has been found


for(let count = 0; count <= 20; count++) {
	// if remainder is equal to 0
	if(count % 2 === 0) {
		// Tells the code to continue to the next iteration
		// This ignores all statements located after the continue statement
		continue;
	}

	console.log("Continue and Break: " + count);

	if(count > 10) {
		// Tells the code to terminate/stop loop even if the expression/condition of the loop defines that should execute so long as the value of count is that or equal to 20
		// number values after 10 will no longer printed
		break;
	}
}

// Loop that will iterate based on the length of the string
// If the vowel is equal to a, continue to the next iteration
// If the current letter is equal to r, stop the loop

let name = "alexandro"

for (let i = 0; i<name.length; i++) {
	if(name[i].toLowerCase()=== "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	console.log(name[i]);

	if(name[i].toLowerCase()=== "d") {
		break;
	}

}