// Array Methods
// JS has built-in functions and methods for arrays
// This allows us to manipulate and access array items
// [SECTION] Mutator Methods
// Mutator methods are functions that mutate or change an array after they are created

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruit'];
console.log("Original Array:");
console.log(fruits);

// push()
// Adds an element in the end of an array and returns the updated array's length
/*
SYNTAX:
	arrayName.push*();
*/

let fruitsLength = fruits.push('Mango');
console.log("fruitsLength: ")
console.log(fruitsLength) //
console.log("Mutated array from push() method:")
console.log(fruits); 

fruitsLength = fruits.push('Avocado', 'Guava');
console.log("fruitsLength: ")
console.log(fruitsLength) //
console.log("Mutated array from push() method:")
console.log(fruits); 

// pop()
// Removes the last element and return the removed element
/*
SYNTAX:
	arrayName.pop();
*/


console.log("Current Array:");
console.log(fruits);

let removedFruit = fruits.pop();

console.log("Removed fruit:");
console.log(removedFruit);
console.log("Mutated array from pop() method:");
console.log(fruits);

// unshift()
// it adds one or more elements at the BEGINNING of an array and it returns the updated array's length
/*
SYNTAX:
	arrayName.unshift('elementA');
	arrayName.unshift('elementA', 'elementB'.....);

*/


console.log("Current Array:");
console.log(fruits);

fruitsLength = fruits.unshift('Lime', 'Banana');

console.log(fruitsLength);
console.log("Mutated array from unshift() method:");
console.log(fruits);


//shift()
// removes an element at the beginning of an array and returns the removed element


/*
SYNTAX:
	arrayName.shift();
*/

console.log("Current Array:");
console.log(fruits);

removedFruit = fruits.shift();

console.log("Removed fruit:");
console.log(removedFruit); //Lime
console.log("Mutated array from the shift() method:");
console.log(fruits);

// splice()
// simultaneously removes an elements from a specified index number and adds element

/*
SYNTAX:
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/


console.log("Current Array:");
console.log(fruits);


fruits.splice(0, 1, 'Pochi');
console.log("Mutated array from splice method:");
console.log(fruits);

// sort()
// rearranges the array element in alphanumeric order
/*
SYNTAX:
	arrayName.sort();
*/

console.log('Current Array:');
console.log(fruits);

fruits.sort();

console.log("Mutated array from sort() method:");
console.log(fruits);

// reverse()
// reverses the order of array elements
/*
SYNTAX:
	arrayName.reverse();
*/

console.log('Current Array:');
console.log(fruits);

fruits.reverse();

console.log("Mutated array from reverse() method:");
console.log(fruits);

// [SECTION] Non-mutator methods
// Non-mutator methods are functions that do not modify or change an array after they are created

let countries = ['PH', 'US', 'SG', 'TH','MY', 'PH', 'FR', 'GR'];

console.log(countries);

// indexOf()
// returns the index number of the first matching element found in an array
/*
SYNTAX:
	arrayName.indexOf(searchValue);
	arrayName.indexOf(searchValue, startingIndex);
*/

let firstIndex = countries.indexOf('VP');

console.log("Result of firstIndex:");
console.log(firstIndex);

firstIndex = countries.indexOf('PH', 1);

console.log("Result of firstIndex:");
console.log(firstIndex);

// If no match was found, the result will be -1




// lastIndexOf()
// returns the index number of the last matching element found in an array
/*
SYNTAX:
	arrayName.lastIndexOf(searchValue);
	arrayName.lastIndexOf(searchValue, startingIndex);
*/

let lastIndex = countries.lastIndexOf('PH');


console.log("Result of lastIndexOf:");
console.log(lastIndex);

// slice
// portions/slices elements from an array and return a new array
/*
SYNTAX:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, endingIndex);

*/

// countries = ['PH', 'US', 'SG', 'TH','MY', 'PH', 'FR', 'GR'];

let slicedArrayA = countries.slice(4);

console.log("Result from slice() method:");
console.log(slicedArrayA);


// The elements that will be sliced are elements from starting index until the element BEFORE the ending index
let slicedArrayB = countries.slice(1, 3);

console.log("Result from slice() method:");
console.log(slicedArrayB);


// toString()
// return an array as string separated by commas
/*
SYNTAX
	arrayName.toString();

*/

// countries = ['PH', 'US', 'SG', 'TH','MY', 'PH', 'FR', 'GR'];

let stringArray = countries.toString();

console.log("Result from toString() method:");
console.log(stringArray);
console.log(typeof stringArray);


// [SECTION] Iteration Methods
// loops designed to perform repetitive task
// loops over all items in an array

// forEach()
// similar to a for loop that iterates on each array of elements

/*
SYNTAX:
	arrayName.forEach(function(indivElement){
		statement
	})

*/

// countries = ['PH', 'US', 'SG', 'TH','MY', 'PH', 'FR', 'GR'];


countries.forEach(function(country){
		console.log(country);
	})

// map()
// iterates on each element and returns new array with different values depending on the result of the function's operation
/*
SYNTAX:
	
	let/const resultArray = arrayNAme.map(function(indivElement) {
	statement
	})

*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
	return number * number;
})

console.log("OriginalArray: ")
console.log(numbers);
console.log("Result of map() method: ")
console.log(numberMap);


// filter()
// returns new array that contains elements which meets the given condition

/*
SYNTAX:
	
	let/const resultArray = arrayNAme.filter(function(indivElement) {
	statement
	})

*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
})

console.log("Result of filter() method:");
console.log(filterValid);



// includes() - Boolean
// checks if the argument passed can be found in the array

/*
SYNTAX:
	arrayName.includes(<argumentToFind>)

*/


let product = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = product.includes("CPU");
console.log("Result from includes() method:");
console.log(productFound1);





