// Use the 'require' directive to load the express module/package

const express = require("express");

// Create an application using express
// "app" is our server
const app = express ();

const port = 3000;

// Method used from express.js are middleware
// middleware is software that provides services and capabilities to application outside of what's offered by the operating system

// allows your app to read json data
app.use(express.json());

// allows your app to read data from any forms
app.use(express.urlencoded({extended: true}));

// [SECTION] Routes
// GET
app.get("/greet", (request, response) => {
	// "response.send" method to send a response back to the client
	response.send("Hello from the /greet endpoint!")
});

// POST

app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

// Simple Registration

let users= [
	{
		username: 'Pochi',
		password: 'Abc123'
	},
	{
		username: 'Tuwi',
		password: '123abC'
	},
	{
		username: 'Ashya',
		password: 'b1d1r1'
	}
];

app.post("/signup", (request, response) => {
	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		response.send(`Please input BOTH username and password!`)
	}
});

// [SECTION] ACTIVITY-s34


// GET route for home page
app.get("/home", (request, response) => {
	response.send(`Welcome to the Home Page!`)
});

// GET route for retrieving the users
app.get("/users", (request, response) => {
	response.send(users);
});


// Delete-user route logic
app.delete("/delete-user", (request, response) => {
	const username = request.body.username;
	let message;

	if (users.length > 0) {
		for (let i = 0; i < users.length; i++) {
			if(users[i].username === username) {
				users.splice(i, 1);
				message = `User ${username} has been deleted.`;
				break;
			}
		}

	if(!message) {
		message = "User does not exist.";
	}

	} else {
		message = "No users found.";
	}
	response.send(message);

});







app.listen(port, () => console.log(`Server running at port ${port}`));