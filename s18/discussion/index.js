// console.log("Hello World!");

// Functions
/*function printInput() {
	let nickname = prompt("Enter your nickname:");
	console.log("Hi, " + nickname);
}
printInput();*/

// Parameters and Arguments
// (name) - is called a parameter di puwede mawala
function printName(name) {
	console.log("My name is " + name);
}

// ("Juana") - the information/data provided directly into the function is called an argument / kapag wala argument undefined lang
printName("Juana");
printName("Pochi");
printName("Panda");
// Variables can also be passed as an argument
let sampleVariable = "Yui"
printName(sampleVariable);

function checkDivisibilityBy8 (num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: "+ remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8)
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(65);
checkDivisibilityBy8(28);

// Functions as Arguments
function argumentFunction() {
	console.log("This function was passed as an argument before the message was printed.")
}

function invokeFunction(argumentFunction) {
	argumentFunction();
}
invokeFunction(argumentFunction);

// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}
// "Juan" will be stored in the parameter "firstName"
// "Dela" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"
createFullName('Juan', 'Dela', 'Cruz');
createFullName('Juan', 'Cruz');


// Using variable as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Wick";

createFullName(firstName, middleName, lastName);

/*function createFullName(middleName, firstName, lastName) {
	console.log(firstName + ' ' + middleName + ' ' + lastName);

createFullName('Juan', 'Dela', 'Cruz');*/